package springTest;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.zh.service.CategoryService;
import com.zh.service.UserService;

public class TestSpring {

/*	public static void main(String[] args) {
		// TODO Auto-generated method stub
           ApplicationContext ac=new ClassPathXmlApplicationContext("application.xml");
           UserService us=(UserService) ac.getBean("userService");
           us.test();
	}*/
	
	
	@Test
	public void test1(){
		  ApplicationContext ac=new ClassPathXmlApplicationContext("application.xml");
          UserService us=(UserService) ac.getBean("userService");
          us.test();
	}

	@Test
	public void test2(){
		  ApplicationContext ac=new ClassPathXmlApplicationContext("application.xml");
          CategoryService cs=(CategoryService) ac.getBean("categoryService");
          cs.findAllFirstCategory();
	}
	
}
