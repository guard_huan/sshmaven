<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE HTML>
<html>
  <head>
    <title>cartlist.jsp</title>   
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<script src="<c:url value='/js/JQuery/jquery-1.12.0.min.js'/>"></script>
	<script src="<c:url value='/js/round.js'/>"></script>	
	<link rel="stylesheet" type="text/css" href="<c:url value='/css/cartitemList.css'/>">
  </head>
  <body>
<c:choose>
	<c:when test="${empty cartItemList }">
	<table width="95%" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td align="right">
				<img align="top" src="<c:url value='/images/icon_empty.png'/>"/>
			</td>
			<td>
				<span class="spanEmpty">您的购物车中暂时没有商品</span>
			</td>
		</tr>
	</table>  
	</c:when>
	<c:otherwise>
<table width="95%" align="center" cellpadding="0" cellspacing="0">
	<tr align="center" bgcolor="#efeae5">
		<td align="left" width="50px">
			<input type="checkbox" id="selectAll" checked="checked"/><label for="selectAll">全选</label>
		</td>
		<td colspan="2">商品名称</td>
		<td>单价</td>
		<td>数量</td>
		<td>小计</td>
		<td>操作</td>
   </tr>

<c:forEach items="${cartItemList }" var="cartItem">
	<tr align="center">
		<td align="left">
			<input value="${cartItem.cartItemId }" type="checkbox" name="checkboxBtn" checked="checked"/>
		</td>
		 <td align="left" width="70px">		 
			<a class="linkImage" href="<c:url value='/jsp/book/desc.jsp'/>"><img border="0" width="54" align="top" src="<c:url value='/${cartItem.book.imageB }'/>"/></a>
		</td> 
		<td align="left" width="400px">
		    <a href="<c:url value='/jsp/book/desc.jsp'/>"><span>${cartItem.book.bname }</span></a>
		</td>
		<td><span>&yen;<span class="currPrice">${cartItem.book.currPrice }</span></span></td>
		<td>
			<a class="jian" href="<c:url value='/deleteQuantity.action?cartItemId=${cartItem.cartItemId }'/>"></a>
			<input class="quantity" readonly="readonly" id="${cartItem.cartItemId }Quantity" type="text" value="${cartItem.quantity }"/>
			<a class="jia" href="<c:url value='/addQuantity.action?cartItemId=${cartItem.cartItemId }'/>"></a>
		</td>
		<td width="100px">
			<span class="price_n">&yen;
			<span class="subTotal" id="${cartItem.cartItemId }Subtotal">${cartItem.subtotal}</span>
			</span>
		</td>
		<td>
			<a href="<c:url value='/deleteCartitem.action?cartItemId=${cartItem.cartItemId }'/>">删除</a>
		</td>
	</tr>
</c:forEach>

	<tr>
		<td colspan="4" class="tdBatchDelete">
			<a href="<c:url value='/deleteAll.action'/>">批量删除</a>
		</td>
		<td colspan="3" align="right" class="tdTotal">
			<span>总计：</span><span class="price_t">&yen;<span id="total"></span></span>
		</td>
	</tr>
	<tr>
		<td colspan="7" align="right">
			<a href="javascript:jiesuanDesc();" id="jiesuan" class="jiesuan"></a>
		</td>
	</tr>	
</table>

	<form id="jieSuanForm" action="<c:url value='/descCartItem.action'/>" method="post">
		<input type="hidden" name="cartItemIds" id="cartItemIds"/>
		<input type="hidden" name="total" id="hiddenTotal"/>
	</form>
	</c:otherwise>
</c:choose>
	<script type="text/javascript">
		$(function() {
			showTotal();
			$("#selectAll").click(function(){
				var bool=$(this).prop("checked");									
				setItemCheckBox(bool);
				jieSuan(bool);
				showTotal();
			});
			$(":checkbox[name=checkboxBtn]").click(function(){
				var all=$(":checkbox[name=checkboxBtn]").length;
				var selected=$("input[name=checkboxBtn]:checked").length;
				if(all==selected){
					$("#selectAll").prop("checked","checked");
					jieSuan(true);	
				}else if(selected==0){
					$("#selectAll").prop("checked",false);
					jieSuan(false);
				}else{
					$("#selectAll").prop("checked",false);
					jieSuan(true);
				}
				showTotal();
			});
		});
		/* 将所有购物条目的复选框的状态设置成一致 */

		function setItemCheckBox(bool) {
			$(":checkbox[name=checkboxBtn]").each(function() {
				$(this).prop("checked", bool);
			});
		}
		/* 给结算按钮设置样式 */
		function jieSuan(bool) {
			if(bool){
				$("#jiesuan").removeClass("kill").addClass("jiesuan");
				$("#jiesuan").unbind("click");
			}else{
				$("#jiesuan").removeClass("jiesuan").addClass("kill");
				$("#jiesuan").bind("click",function(){
					return false;
				});
			}

		}
		/* 计算总计 */
		function showTotal() {
			var total = 0;
			$(":checkbox[name=checkboxBtn]").each(function() {
				var bool=$(this).prop("checked");
				if(bool){
					var id = $(this).val();
					var text = $("#" + id + "Subtotal").text();
					total += Number(text);
				}				
			});
			$("#total").text(round(total,2));
		}
		/* 点击结算按钮触发的事件 */
		function jiesuanDesc(){
			var cartItemIdArray=new Array();
			$("input[name=checkboxBtn]:checked").each(function(){
				cartItemIdArray.push($(this).val());
			});
			$("#cartItemIds").val(cartItemIdArray.toString());
			$("#hiddenTotal").val($("#total").text());
			$("#jieSuanForm").submit();
		}
	</script>
</body>
</html>
