$(function(){
	/*提交按钮切换图片的效果*/
	$("#submitBtn").hover(function(){
		$("#submitBtn").attr("src","/sshmaven/images/login1.jpg");
	},function(){
		$("#submitBtn").attr("src","/sshmaven/images/login2.jpg");
	});
	/*文本框失去焦点时触发的事件*/
	$(".input").blur(function(){
		var id=$(this).attr("id");
		var funName="validate"+id.substring(0,1).toUpperCase()+id.substring(1)+"()";
		eval(funName);
	});
	$(".input").focus(function(){
		var id=$(this).attr("id");
		var labelId=id+"Error";
		$("#"+labelId).text("");
		showError($("#"+labelId));
	});
	/*注册按钮提交时触发的事件*/
	$("#loginForm").submit(function(){
		var bool=true;
		if(!validateLoginname()){
			bool=false;
		}
		if(!validateLoginpass()){
			bool=false;
		}
		if(!validateVerifyCode()){
			bool=false;
		}
		
		return bool;
	});
});
function validateLoginname(){
	/*校验用户名是否为空*/
	var bool=true;
	var value=$("#loginname").val();
	if(!value){
		$("#loginnameError").text("用户名不能为空");
		showError($("#loginnameError"));
		bool=false;
		/*return false;*/
	}
	/*校验用户名的长度*/
	if(value.length<3 || value.length>20){
		$("#loginnameError").text("用户名的长度必须在3~20之间");
		showError($("#loginnameError"));
		bool=false;
		/*return false;*/
	}
	/*校验用户名是否不存在*/
	$.ajax({
		type:'post',
		data:'loginname='+value,
		url:'./validateLoginname.action',
		dataType:'JSON',
		async:false,
		cache:false,
		success:function(data){
			if(!data){
				/*alert("success函数执行了");*/
				$("#loginnameError").text("用户名不存在!!!");
				showError($("#loginnameError"));
				bool=false;
				/*return false;*/
			}			
		}
	});	
	return bool;
}
/*校验密码*/
function validateLoginpass(){
	var bool=true;
	/*校验密码是否为空*/
	var loginname=$("#loginname").val();
	var value=$("#loginpass").val();
	if(!value){
		$("#loginpassError").text("密码不能为空");
		showError($("#loginpassError"));
		bool=false;
	}
	/*校验密码的长度*/
	if(value.length<3 || value.length>20){
		$("#loginpassError").text("密码的长度必须在3~20之间");
		showError($("#loginpassError"));
		bool=false;
	}	
	/*校验密码是否正确*/
	$.ajax({
		type:'post',
		data:{"loginname":loginname,"loginpass":value},
		url:'./validateLoginpass.action',
		dataType:'JSON',
		async:false,
		cache:false,
		success:function(data){
			if(!data){
				/*alert("success函数执行了");*/
				$("#loginpassError").text("密码不正确!!!");
				showError($("#loginpassError"));
				bool=false;
				/*return false;*/
			}			
		}
	});	
	return bool;
}
/*验证码的校验*/
function validateVerifyCode(){
	var bool=true;
	/*校验验证码是否为空*/
	var value=$("#verifyCode").val();
	if(!value){
		$("#verifyCodeError").text("验证码不能为空");
		showError($("#verifyCodeError"));
		bool=false;
	}
	
/*校验验证码输入是否正确*/
	$.ajax({
		type:'post',
		data:'verifyCode='+value,
		url:'./validateCode.action',
		dataType:'JSON',
		async:false,
		cache:false,
		success:function(data){
			if(!data){
				$("#verifyCodeError").text("验证码输入有误");
				showError($("#verifyCodeError"));
				bool=false;
			}			
		}
	});	
	return bool;
}

/*当label标签有内容时才显示label标签的样式 ，ele为$("#labelId")*/
function showError(ele){
	var text=ele.text();
	if(text){/*label标签有内容时就显示*/
		ele.css("display","");
	}else{
	    ele.css("display","none");
	}
}