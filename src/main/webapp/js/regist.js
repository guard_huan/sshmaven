$(function(){
	//页面开始加载时取消label标签的样式
	$(".error").css("display","none");
	
	/*注册按钮切换图片的效果*/
	$("#submitBtn").hover(function(){
		$("#submitBtn").attr("src","/sshmaven/images/regist2.jpg");
	},function(){
		$("#submitBtn").attr("src","/sshmaven/images/regist1.jpg");
	});
	
	/*文本框失去焦点触发对应的校验方法*/
	$(".input").blur(function(){
		var id=$(this).attr("id");
		var funName="validate"+id.substring(0,1).toUpperCase()+id.substring(1)+"()";
		eval(funName);
	});
	
	/*文本框获得焦点时触发的事件*/
	$(".input").focus(function(){
		var id=$(this).attr("id");
		var labelId=id+"Error";
		$("#"+labelId).text("");
		showError($("#"+labelId));
	});
	
	/*提交按钮触发的校验,
	 * 其实在按提交按钮之前也执行了一次文本框的blur事件，之后再执行submit事件，如果前面的校验有问题，则返回false，阻止提交
	 * */
	$("#registForm").submit(function(){
		var bool=true;
		if(!validateLoginname()){
			/*alert("用户名验证了");*/
			bool=false;
		}
		if(!validateLoginpass()){
			/*alert("密码验证了");*/
			bool=false;
		}
		if(!validateReloginpass()){
			/*alert("重复密码验证了");*/
			bool=false;
		}
		if(!validateEmail()){
			/*alert("邮箱验证了");*/
			bool=false;
		}
		if(!validateVerifyCode()){
			/*alert("验证码验证了");*/
			bool=false;
		}
		return bool;
	});
});

/*验证用户名*/
function validateLoginname(){
	/*校验用户名是否为空*/
	var bool=true;
	var value=$("#loginname").val();
	if(!value){
		$("#loginnameError").text("用户名不能为空");
		showError($("#loginnameError"));
		bool=false;
		/*return false;*/
	}
	/*校验用户名的长度*/
	if(value.length<3 || value.length>20){
		$("#loginnameError").text("用户名的长度必须在3~20之间");
		showError($("#loginnameError"));
		bool=false;
		/*return false;*/
	}
	/*校验用户名是否注册*/
	$.ajax({
		type:'post',
		data:'loginname='+value,
		url:'./validateLoginname.action',
		dataType:'JSON',
		async:false,
		cache:false,
		success:function(data){
			if(data){
				/*alert("success函数执行了");*/
				$("#loginnameError").text("用户名已经注册");
				showError($("#loginnameError"));
				bool=false;
				/*return false;*/
			}			
		}
	});	
	return bool;
}
/*校验密码*/
function validateLoginpass(){
	/*校验密码是否为空*/
	var value=$("#loginpass").val();
	if(!value){
		$("#loginpassError").text("密码不能为空");
		showError($("#loginpassError"));
		return false;
	}
	/*校验密码的长度*/
	if(value.length<3 || value.length>20){
		$("#loginpassError").text("密码的长度必须在3~20之间");
		showError($("#loginpassError"));
		return false;
	}	
	
	return true;
}

/*确认密码的校验*/
function validateReloginpass(){
	/*校验确认密码是否为空*/
	var value=$("#reloginpass").val();
	if(!value){
		$("#reloginpassError").text("确认密码不能为空");
		showError($("#reloginpassError"));
		return false;
	}
	/*两次密码是否一致*/
	if(value!=$("#loginpass").val()){
		$("#reloginpassError").text("两次密码不一致");
		showError($("#reloginpassError"));
		return false;
	}	
	
	return true;
}
/*邮箱的校验*/
function validateEmail(){
	var bool=true;
	/*校验邮箱是否为空*/
	var value=$("#email").val();
	if(!value){
		$("#emailError").text("邮箱不能为空");
		showError($("#emailError"));
		bool=false;
	}
	/*邮箱格式是否正确*/
	if(!/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/.test(value)){
		$("#emailError").text("邮箱格式不正确");
		showError($("#emailError"));
		bool=false;
	}	
	/*校验邮箱是否注册*/
	$.ajax({
		type:'post',
		data:'email='+value,
		url:'./validateEmail.action',
		dataType:'JSON',
		async:false,
		cache:false,
		success:function(data){
			if(data){
				/*alert("success函数执行了");*/
				$("#emailError").text("该邮箱已经注册");
				showError($("#emailError"));
				bool=false;
			}			
		}
	});	
	
	return bool;
}

/*验证码的校验*/
function validateVerifyCode(){
	var bool=true;
	/*校验验证码是否为空*/
	var value=$("#verifyCode").val();
	if(!value){
		$("#verifyCodeError").text("验证码不能为空");
		showError($("#verifyCodeError"));
		bool=false;
	}
	
/*校验验证码输入是否正确*/
	$.ajax({
		type:'post',
		data:'verifyCode='+value,
		url:'./validateCode.action',
		dataType:'JSON',
		async:false,
		cache:false,
		success:function(data){
			if(!data){
				$("#verifyCodeError").text("验证码输入有误");
				showError($("#verifyCodeError"));
				bool=false;
			}			
		}
	});	
	return bool;
}

/*当label标签有内容时才显示label标签的样式 ，ele为$("#labelId")*/
function showError(ele){
	var text=ele.text();
	if(text){/*label标签有内容时就显示*/
		ele.css("display","");
	}else{
	    ele.css("display","none");
	}
}