<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
  <head>
    <title>left</title>
    <base target="body"/>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<meta http-equiv="content-type" content="text/html;charset=utf-8">
	
	<!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<c:url value='/thirdparty/adminlte/bootstrap/css/bootstrap.min.css'/>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdn.bootcss.com/font-awesome/4.4.0/css/font-awesome.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdn.bootcss.com/ionicons/2.0.1/css/ionicons.min.css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="<c:url value='/thirdparty/adminlte/dist/css/AdminLTE.min.css'/>">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="<c:url value='/thirdparty/adminlte/dist/css/skins/skin-blue.min.css'/>">
</head>
  
<body>  
<!-- class="main-sidebar" -->
      <aside >
      
        <!-- sidebar: style can be found in sidebar.less -->
        
        <section class="sidebar">               
          <!-- Sidebar Menu -->
          
          <ul class="sidebar-menu">
            <li class="header">书单分类</li>   
                                  
            <c:forEach items="${parents}" var="parent">
            <li class="treeview">
              <a href="#"><i class="fa fa-link"></i> <span>${parent.cname }</span> <i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <c:forEach items="${parent.categories}" var="child">
                   <li><a href="<c:url value='/findBookByCid.action?cid=${child.cid}'/>" target="body">${child.cname }</a></li>
                </c:forEach>
              </ul>
            </li>
            </c:forEach>          
          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>
      
    <!-- jQuery 2.1.4 -->
    <script src="<c:url value='/thirdparty/adminlte/plugins/jQuery/jQuery-2.1.4.min.js'/>"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<c:url value='/thirdparty/adminlte/bootstrap/js/bootstrap.min.js'/>"></script>
    <!-- AdminLTE App -->
    <script src="<c:url value='/thirdparty/adminlte/dist/js/app.min.js'/>"></script>
</body>
</html>
