package com.zh.action;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.zh.pager.PageBean;
import com.zh.pager.PageConstants;
import com.zh.pojo.Cartitem;
import com.zh.pojo.Order;
import com.zh.pojo.Orderitem;
import com.zh.pojo.User;
import com.zh.service.CartitemService;
import com.zh.service.OrderService;

public class OrderAction {
	private String oid;
	private CartitemService cartitemService;
	private OrderService orderService;

	
	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public CartitemService getCartitemService() {
		return cartitemService;
	}

	public void setCartitemService(CartitemService cartitemService) {
		this.cartitemService = cartitemService;
	}

	public OrderService getOrderService() {
		return orderService;
	}

	public void setOrderService(OrderService orderService) {
		this.orderService = orderService;
	}
	/*获取请求的URL，但是要除去pc请求参数
	 * 1.无pc请求参数
	 * 2.pc请求参数在最后一个
	 * 3.pc请求参数在中间
	 * 
	 * */
	public String getUrl(){
		HttpServletRequest request=ServletActionContext.getRequest();
		String url=request.getRequestURL()+"?"+request.getQueryString();
		int fromIndex=url.lastIndexOf("&pc=");
		if(fromIndex==-1){//说明原始的请求路径中不带查询pc查询条件
			return url;
		}
		int toIndex=url.indexOf("&",fromIndex+1);//从指定的索引处开始找指定的字符串在此字符串中首次出现的索引位置
		if(toIndex==-1)
			return url.substring(0, fromIndex);
		return url.substring(0, fromIndex)+url.substring(toIndex);
	}
	/*获取当前分页号pc*/
	public int getPc(){
		HttpServletRequest request=ServletActionContext.getRequest();
		String pageCode=request.getParameter("pc");//URL中有传递pc过来
		int pc;
		if(pageCode==null){
			pc=1;
		}else{
			pc=Integer.parseInt(pageCode);
		}
		return pc;
	}
	public String myOrder(){
		HttpSession session=ServletActionContext.getRequest().getSession();
		int pc=this.getPc();
    	User user=(User) session.getAttribute("sessionUser");
    	PageBean<Order> pb=orderService.findByUid(user.getUid(), pc);
    	pb.setPc(pc);
    	pb.setPs(PageConstants.ORDER_PAGE_SIZE);
    	pb.setUrl(this.getUrl());
    	session.setAttribute("pb", pb);
    	return "success";
	}
	public String addOrder(){
		HttpSession session=ServletActionContext.getRequest().getSession();
		User user=(User) session.getAttribute("sessionUser");
		HttpServletRequest request=ServletActionContext.getRequest();	
		String address=request.getParameter("address");
		String cartItemIds=request.getParameter("cartItemIds");
		String[] Ids=cartItemIds.split(",");
		Order order=new Order();	
		order.setOid(UUID.randomUUID().toString().replaceAll("\\-", ""));
		order.setOrdertime(String.format("%tF %<tT", new java.util.Date()));
		order.setStatus(1);
		order.setAddress(address);
		order.setUser(user);
		List<Orderitem> orderitems = new ArrayList<Orderitem>();
		double money=0;
		for(int i=0;i<Ids.length;i++){	
			Cartitem cartitem=cartitemService.findByCartitemId(Ids[i]);
			money+=cartitem.getSubtotal();
			Orderitem orderitem=new Orderitem();
			orderitem.setOrderItemId(UUID.randomUUID().toString().replaceAll("\\-", ""));
			orderitem.setQuantity(cartitem.getQuantity());
			orderitem.setBid(cartitem.getBook().getBid());
			orderitem.setBname(cartitem.getBook().getBname());
			orderitem.setCurrPrice(cartitem.getBook().getCurrPrice());
			orderitem.setImageB(cartitem.getBook().getImageB());
			orderitem.setOrder(order);
			orderitem.setSubtotal(cartitem.getSubtotal());
			orderitems.add(orderitem);
			System.out.println("orderitemId:"+orderitem.getOrderItemId());
			cartitemService.delete(Ids[i]);
		}
		order.setTotal(money);
		order.setOrderitems(orderitems);
		orderService.save(order);
		System.out.println("orderId:"+order.getOid());
		return "success";
	}
	
	/**
	 * 
	 * @return String
	 * @description  TODO 按照订单编号查找订单的详细信息
	 * @date    2016年7月28日上午9:30:10
	 */
	public String findOrderByOid(){
		HttpSession session=ServletActionContext.getRequest().getSession();
		HttpServletRequest request=ServletActionContext.getRequest();
		String btn=request.getParameter("btn");
		session.setAttribute("btn", btn);
		Order order=orderService.findByOid(oid);
		session.setAttribute("order",order);
		return "success";
	}
	
	
	/**
	 * 
	 * @return String
	 * @description  TODO 取消订单，实质就是将订单的未付款状态修改为取消状态
	 * @date    2016年7月28日下午3:23:41
	 */
	public String cancelOrder(){
		HttpSession session=ServletActionContext.getRequest().getSession();
		Order order=orderService.findByOid(oid);
		if(order.getStatus()==1){
			order.setStatus(5);
			orderService.updateOrder(order);
			session.setAttribute("code", "success");
			session.setAttribute("msg", "订单已取消");
		}else{
			session.setAttribute("code","error");
			session.setAttribute("msg", "你已付款，不能取消订单");
		}
		return "success";
	}
	
	/**
	 * 
	 * @return String
	 * @description  TODO 确认收货，其实就是将订单从已发货状态变为交易成功状态
	 * @date    2016年7月28日下午4:39:04
	 */
	public String confirm(){
		HttpSession session=ServletActionContext.getRequest().getSession();
		Order order=orderService.findByOid(oid);
		if(order.getStatus()==3){
			order.setStatus(4);
			orderService.updateOrder(order);
			session.setAttribute("code", "success");
			session.setAttribute("msg", "确认收货");
		}else{
			session.setAttribute("code","error");
			session.setAttribute("msg", "还没有发货，您不能确认收货");
		}
		return "success";
	}
}
