package com.zh.action;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.zh.pojo.Book;
import com.zh.pojo.Cartitem;
import com.zh.pojo.User;
import com.zh.service.BookService;
import com.zh.service.CartitemService;


public class CartitemAction {
	private String cartItemId;
		
	private BookService bookService;
    private CartitemService cartitemService;   

	public String getCartItemId() {
		return cartItemId;
	}

	public void setCartItemId(String cartItemId) {
		this.cartItemId = cartItemId;
	}

	public BookService getBookService() {
		return bookService;
	}

	public void setBookService(BookService bookService) {
		this.bookService = bookService;
	}

	public CartitemService getCartitemService() {
		return cartitemService;
	}

	public void setCartitemService(CartitemService cartitemService) {
		this.cartitemService = cartitemService;
	}
    public String myCart(){
    	HttpSession session=ServletActionContext.getRequest().getSession();
    	User user=(User) session.getAttribute("sessionUser");
    	List<Cartitem> cartItemList=cartitemService.myCart(user.getUid());
    	session.setAttribute("cartItemList", cartItemList);
    	return "success";
    }
    public String descCartItem(){
    	HttpServletRequest request=ServletActionContext.getRequest();
    	String cartItemIds=request.getParameter("cartItemIds");
    	String total=request.getParameter("total");
    	String[] Ids=cartItemIds.split(",");
    	List<Cartitem> cartItemList=new ArrayList<Cartitem>();
    	for(int i=0;i<Ids.length;i++){
    		Cartitem cartitem=cartitemService.findByCartitemId(Ids[i]);
    		cartItemList.add(cartitem);
    	}
    	request.setAttribute("cartItemIds", cartItemIds);
    	request.setAttribute("cartItemList", cartItemList);
    	request.setAttribute("total", total);
    	return "success";
    }
    
	public String addCartItem(){
    	HttpSession session=ServletActionContext.getRequest().getSession();
    	HttpServletRequest request=ServletActionContext.getRequest();
    	User user=(User) session.getAttribute("sessionUser");
    	String bid=request.getParameter("bid");
    	String quan=request.getParameter("quantity");
    	Integer quantity=Integer.parseInt(quan);
    	Cartitem cartitem=cartitemService.findByUidAndBid(user.getUid(), bid);   	
    	if(cartitem!=null){   
    		cartitem.setQuantity(quantity+cartitem.getQuantity());
    		cartitemService.update(cartitem);
    	}else{
    		Cartitem cartitem2=new Cartitem();		
    		Book book=bookService.findByBid(bid);
    		cartitem2.setCartItemId(UUID.randomUUID().toString().replaceAll("\\-", "")); 	 
    		cartitem2.setBook(book);
    		cartitem2.setUser(user);
    		cartitem2.setQuantity(quantity);
    		cartitemService.save(cartitem2);
    	}  	
    	return "success";
    }
	
	public String deleteAll(){
		HttpSession session=ServletActionContext.getRequest().getSession();
		User user=(User) session.getAttribute("sessionUser");
		cartitemService.deleteAll(user.getUid());
		return "success";
	}
	
	public String deleteCartitem(){
		HttpServletRequest request=ServletActionContext.getRequest();
		String cartItemId=request.getParameter("cartItemId");
		cartitemService.delete(cartItemId);
		return "success";
	}
	public String addQuantity(){
		Cartitem cartitem=cartitemService.findByCartitemId(cartItemId);
		cartitem.setQuantity(cartitem.getQuantity()+1);
		cartitemService.update(cartitem);
		return "success";
	}
	public String deleteQuantity(){
		Cartitem cartitem=cartitemService.findByCartitemId(cartItemId);
		if(cartitem.getQuantity()==1){
			cartitemService.delete(cartitem.getCartItemId());
		}else{
			cartitem.setQuantity(cartitem.getQuantity()-1);
			cartitemService.update(cartitem);
		}	
		return "success";
	} 
}
