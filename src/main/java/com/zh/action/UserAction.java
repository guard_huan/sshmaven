package com.zh.action;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.zh.pojo.User;
import com.zh.service.UserService;
import com.zh.util.IdentifyingCode;
import com.zh.util.mail.Mail;
import com.zh.util.mail.MailUtils;

public class UserAction {
	private String loginname;
	private String loginpass;
    private String email;
    private String verifyCode;
    
	
	private UserService userService;

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	
    public String getLoginname() {
		return loginname;
	}

	public void setLoginname(String loginname) {
		this.loginname = loginname;
	}

	public String getLoginpass() {
		return loginpass;
	}

	public void setLoginpass(String loginpass) {
		this.loginpass = loginpass;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getVerifyCode() {
		return verifyCode;
	}

	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}

	/*测试struts2框架*/
	public String test() {
		System.out.println("SSSSS");
		return "success";
	}
    public String register() throws MessagingException, IOException{
    	User user=new User();
    	user.setUid(UUID.randomUUID().toString().replaceAll("\\-", ""));
    	user.setLoginname(loginname);
    	user.setLoginpass(loginpass);
    	user.setEmail(email);
    	user.setStatus(false);
    	user.setActivationCode(UUID.randomUUID().toString().replaceAll("\\-", ""));
    	userService.add(user);
    	//注册成功后像用户发送邮件
        Properties prop=new Properties();
        prop.load(this.getClass().getClassLoader().getResourceAsStream("/email_template.properties"));
        String host=prop.getProperty("host");   
        String username=prop.getProperty("username");
        String password=prop.getProperty("password");
        String from=prop.getProperty("from");
        String to=user.getEmail();
        String subject=prop.getProperty("subject");
        String content=MessageFormat.format(prop.getProperty("content"), user.getActivationCode());
        Session session=MailUtils.createSession(host,username,password);		
		Mail mail=new Mail(from,to,subject,content);		
		MailUtils.send(session,mail);
    	return "success";
    }
    /*用户激活*/
    public String activation(){
    	HttpServletRequest request=ServletActionContext.getRequest();
    	String code=request.getParameter("activationCode");
        List<User> list=userService.findByActivationCode(code);
    	User user=list.get(0);
    	System.out.println(user.getLoginname());
    	if(user.getActivationCode().equals(code)){
    		user.setStatus(true);
    		userService.update(user);
    		System.out.println(user.getStatus());
    	}
    	return "success";
    }
	/*校验用户名是否注册*/
    public void validateLoginname() throws IOException{
    	HttpServletResponse response=ServletActionContext.getResponse();
    	List<User> listUser=userService.findByName(loginname);
    	if(listUser!=null && listUser.size()>0){
    		response.getWriter().println(true);
    	}else{
    		response.getWriter().println(false);
    	}
    }
    /*校验邮箱是否注册*/
    public void validateEmail() throws IOException{
    	HttpServletResponse response=ServletActionContext.getResponse();
    	List<User> listUser=userService.findByEmail(email);
    	if(listUser!=null && listUser.size()>0){
    		response.getWriter().println(true);
    	}else{
    		response.getWriter().println(false);
    	}
    }
    /*异步校验密码是否正确*/
   public void validateLoginpass() throws IOException{
	   HttpServletResponse response=ServletActionContext.getResponse();
	   List<User> listUser=userService.findByName(loginname);
	   if(listUser.get(0).getLoginpass().equals(loginpass)){
		   response.getWriter().println(true);
	   }else{
		   response.getWriter().println(false);
	   }
   }
    /*校验验证码输入是否正确*/
    public void validateCode() throws IOException{
    	userService.validateCode(verifyCode);
    }
	public String login() {
		HttpServletRequest request=ServletActionContext.getRequest();
		HttpSession session=ServletActionContext.getRequest().getSession();
		List<User> listUser=userService.findByName(loginname);
		User user=listUser.get(0);
		if(user.getLoginpass().equals(loginpass)){
			if(verifyCode.equalsIgnoreCase((String) session.getAttribute("code"))){
				session.setAttribute("sessionUser", user);
				request.setAttribute("msg", "登陆成功!!!");
				return "success";
			}
			return "false";
		}else{
			return "false";
		}
		
	}
	//修改用户密码
	public String updatePass(){
		HttpServletRequest request=ServletActionContext.getRequest();
		HttpSession session=ServletActionContext.getRequest().getSession();
		String newpass=request.getParameter("newpass");
		User user=(User) session.getAttribute("sessionUser");
		user.setLoginpass(newpass);
		userService.update(user);
		
		return "success";
	}
   public String quit(){
	   HttpSession session=ServletActionContext.getRequest().getSession();
	   session.removeAttribute("sessionUser");
	   return "success";
   }
	/* 在屏幕上输出验证码 */
	public void printCode() {
		HttpSession session=ServletActionContext.getRequest().getSession();
		HttpServletResponse response=ServletActionContext.getResponse();
		String code;//存储验证码
		response.setContentType("image/jpeg");
		IdentifyingCode idCode = new IdentifyingCode();
		BufferedImage image = new BufferedImage(idCode.getWidth(), idCode.getHeight(), BufferedImage.TYPE_INT_BGR);
		Graphics2D g = image.createGraphics();
		// 定义字体样式
		Font myFont = new Font("黑体", Font.BOLD, 16);
		// 设置字体
		g.setFont(myFont);

		g.setColor(idCode.getRandomColor(200, 250));
		// 绘制背景
		g.fillRect(0, 0, idCode.getWidth(), idCode.getHeight());

		g.setColor(idCode.getRandomColor(180, 200));
		idCode.drawRandomLines(g, 160);
		code = idCode.drawRandomString(4, g);
		session.setAttribute("code", code);
		System.out.println(code);
		g.dispose();
		try {
			ImageIO.write(image, "JPEG", response.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
