package com.zh.action;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.zh.pager.PageBean;
import com.zh.pager.PageConstants;
import com.zh.pojo.Book;
import com.zh.service.BookService;

public class BookAction {	
	private BookService bookService;

	public BookService getBookService() {
		return bookService;
	}

	public void setBookService(BookService bookService) {
		this.bookService = bookService;
	}
    
	/*获取请求的URL，但是要除去pc请求参数
	 * 1.无pc请求参数
	 * 2.pc请求参数在最后一个
	 * 3.pc请求参数在中间
	 * 
	 * */
	public String getUrl(){
		HttpServletRequest request=ServletActionContext.getRequest();
		String url=request.getRequestURL()+"?"+request.getQueryString();
		int fromIndex=url.lastIndexOf("&pc=");
		if(fromIndex==-1){//说明原始的请求路径中不带查询pc查询条件
			return url;
		}
		int toIndex=url.indexOf("&",fromIndex+1);//从指定的索引处开始找指定的字符串在此字符串中首次出现的索引位置
		if(toIndex==-1)
			return url.substring(0, fromIndex);
		return url.substring(0, fromIndex)+url.substring(toIndex);
	}
	
	/*获取当前分页号pc*/
	public int getPc(){
		HttpServletRequest request=ServletActionContext.getRequest();
		String pageCode=request.getParameter("pc");//URL中有传递pc过来
		int pc;
		if(pageCode==null){
			pc=1;
		}else{
			pc=Integer.parseInt(pageCode);
		}
		return pc;
	}
	
	/*按照图书分类号查找图书列表*/
	public String findBookByCid(){
		HttpServletRequest request=ServletActionContext.getRequest();
		String cid=request.getParameter("cid");//URL中有传递cid参数过来		
		int pc=this.getPc();
		PageBean<Book> pageBean=new PageBean<Book>();
		pageBean=bookService.findByCid(cid, pc);		
		pageBean.setPc(pc);
		pageBean.setPs(PageConstants.BOOK_PAGE_SIZE);
		pageBean.setUrl(this.getUrl());
		request.setAttribute("pb", pageBean);
		return "success";
	}
	
	public String findByAuthor(){
		HttpServletRequest request=ServletActionContext.getRequest();
		String author=request.getParameter("author");//URL中有传递author参数过来		
		System.out.println(author);
		int pc=this.getPc();
		System.out.println(pc);
		PageBean<Book> pageBean=new PageBean<Book>();
		pageBean=bookService.findByAuthor(author, pc);		
		pageBean.setPc(pc);
		pageBean.setPs(PageConstants.BOOK_PAGE_SIZE);
		pageBean.setUrl(this.getUrl());
	    System.out.println(pageBean.getTp());
		request.setAttribute("pb", pageBean);
		return "success";
	}
	
	public String findByPress(){
		HttpServletRequest request=ServletActionContext.getRequest();
		String press=request.getParameter("press");//URL中有传递press参数过来		
		
		int pc=this.getPc();
		
		PageBean<Book> pageBean=new PageBean<Book>();
		pageBean=bookService.findByPress(press, pc);		
		pageBean.setPc(pc);
		pageBean.setPs(PageConstants.BOOK_PAGE_SIZE);
		pageBean.setUrl(this.getUrl());
	    
		request.setAttribute("pb", pageBean);
		return "success";
	}
	
	/*按照图书编号查找图书，其实是查找图书的详细信息*/
	public String findByBid(){
		HttpServletRequest request=ServletActionContext.getRequest();
		String bid=request.getParameter("bid");//从请求链接中获取参数的值	
		Book book=bookService.findByBid(bid);
		request.setAttribute("book", book);
		return "success";
	}
}
