package com.zh.action;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.zh.pojo.Category;
import com.zh.service.CategoryService;

public class CategoryAction {
	private String cid;
    public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}
	private CategoryService categoryService;

	public CategoryService getCategoryService() {
		return categoryService;
	}

	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}
	
	public String category(){
		HttpServletRequest request=ServletActionContext.getRequest();
		List<Category> parents=categoryService.findAllFirstCategory();
		request.setAttribute("parents", parents);
		return "success";
	}
	
    
}
