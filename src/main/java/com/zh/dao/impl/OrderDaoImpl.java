package com.zh.dao.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.SessionFactory;

import com.zh.dao.OrderDao;
import com.zh.pojo.Order;

public class OrderDaoImpl extends BaseDaoImpl<Order> implements OrderDao {
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
