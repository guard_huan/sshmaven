package com.zh.dao.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.SessionFactory;

import com.zh.dao.CategoryDao;
import com.zh.pojo.Category;

public class CategoryDaoImpl extends BaseDaoImpl<Category> implements CategoryDao {
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	

}
