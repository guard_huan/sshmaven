package com.zh.dao.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.zh.dao.BaseDao;

public class BaseDaoImpl<T> implements BaseDao<T> {
    private SessionFactory sessionFactory;
    
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public Session getCurrentSession(){
		return this.getSessionFactory().getCurrentSession();
	}
	@Override
	public Serializable save(T o) {
		return this.getCurrentSession().save(o);
	}

	@Override
	public void delete(T o) {
		this.getCurrentSession().delete(o);
		
	}

	@Override
	public void update(T o) {
		this.getCurrentSession().update(o);
		
	}

	@Override
	public void saveOrUpdate(T o) {
		this.getCurrentSession().saveOrUpdate(o);
		
	}

	@Override
	public List<T> find(String sql) {
		Query q=this.getCurrentSession().createQuery(sql);	
		return q.list();
	}

	@Override
	public List<T> find(String sql, Map<String, Object> params) {
		Query q=this.getCurrentSession().createQuery(sql);
		if(params!=null && !params.isEmpty()){
			for(String key:params.keySet()){
				q.setParameter(key, params.get(key));
			}
		}
		return q.list();
	}

	@Override
	public List<T> find(String sql, int page, int rows) {
		Query q=this.getCurrentSession().createQuery(sql);
		q.setFirstResult((page-1)*rows);
		q.setMaxResults(rows);
		return q.list();
	}

	@Override
	public List<T> find(String sql, Map<String, Object> params, int page, int rows) {
		Query q=this.getCurrentSession().createQuery(sql);
		if(params!=null && !params.isEmpty()){
			for(String key:params.keySet()){
				q.setParameter(key, params.get(key));
			}
		}
		//q.setFirstResult((page-1)*rows).setMaxResults(rows).list();这样三句和在一起也可以
		q.setFirstResult((page-1)*rows);
		q.setMaxResults(rows);
		return q.list();
	}

}
