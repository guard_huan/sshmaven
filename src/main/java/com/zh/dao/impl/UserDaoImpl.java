package com.zh.dao.impl;
import java.io.Serializable;
import org.hibernate.SessionFactory;
import com.zh.dao.UserDao;
import com.zh.pojo.User;

public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao {
    private SessionFactory sessionFactory;
    
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public Serializable add(User user) {
		return this.sessionFactory.getCurrentSession().save(user);
		
	}

	

}
