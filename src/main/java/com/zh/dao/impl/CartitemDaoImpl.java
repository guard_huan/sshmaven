package com.zh.dao.impl;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.SessionFactory;

import com.zh.dao.CartitemDao;
import com.zh.pojo.Cartitem;

public class CartitemDaoImpl extends BaseDaoImpl<Cartitem> implements CartitemDao {
    private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
    
}
