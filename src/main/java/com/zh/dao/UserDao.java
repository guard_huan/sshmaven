package com.zh.dao;

import java.io.Serializable;
import com.zh.pojo.User;

public interface UserDao extends BaseDao<User>{
   public Serializable add(User user);
}
