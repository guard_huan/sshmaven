package com.zh.dao;

import java.util.List;
import java.util.Set;

import com.zh.pojo.Category;

public interface CategoryDao extends BaseDao<Category> {
	
}
