package com.zh.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface BaseDao<T>{
     public Serializable save(T o);
     public void delete(T o);
     public void update(T o);
     public void saveOrUpdate(T o);
     public List<T> find(String sql);
     public List<T> find(String sql,Map<String,Object> params);
     public List<T> find(String sql,int page,int rows);//page表示第几页，rows表示一页显示的最多条数
     public List<T> find(String sql,Map<String,Object> params,int page,int rows);
 }
