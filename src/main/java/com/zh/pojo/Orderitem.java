package com.zh.pojo;



/**
 * Orderitem entity. @author MyEclipse Persistence Tools
 */

public class Orderitem {


    // Fields    

     private String orderItemId;
     private Order order;
     private Integer quantity;
     private double subtotal;
     private String bid;
     private String bname;
     private double currPrice;
     private String imageB;


    // Constructors

    /** default constructor */
    public Orderitem() {
    }

    
    /** full constructor */
    public Orderitem(Order order, Integer quantity, double subtotal, String bid, String bname, double currPrice, String imageB) {
        this.order = order;
        this.quantity = quantity;
        this.subtotal = subtotal;
        this.bid = bid;
        this.bname = bname;
        this.currPrice = currPrice;
        this.imageB = imageB;
    }

   
    // Property accessors

    public String getOrderItemId() {
        return this.orderItemId;
    }
    
    public void setOrderItemId(String orderItemId) {
        this.orderItemId = orderItemId;
    }

    public Order getOrder() {
        return this.order;
    }
    
    public void setOrder(Order order) {
        this.order = order;
    }

    public Integer getQuantity() {
        return this.quantity;
    }
    
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public double getSubtotal() {
        return this.subtotal;
    }
    
    public void setSubtotal(double subtotal) {
        this.subtotal = subtotal;
    }

    public String getBid() {
        return this.bid;
    }
    
    public void setBid(String bid) {
        this.bid = bid;
    }

    public String getBname() {
        return this.bname;
    }
    
    public void setBname(String bname) {
        this.bname = bname;
    }

    public double getCurrPrice() {
        return this.currPrice;
    }
    
    public void setCurrPrice(double currPrice) {
        this.currPrice = currPrice;
    }

    public String getImageB() {
        return this.imageB;
    }
    
    public void setImageB(String imageB) {
        this.imageB = imageB;
    }
   








}