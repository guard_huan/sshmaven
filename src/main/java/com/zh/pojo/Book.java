package com.zh.pojo;

import java.util.HashSet;
import java.util.Set;


/**
 * Book entity. @author MyEclipse Persistence Tools
 */

public class Book  implements java.io.Serializable {


    // Fields    

     private String bid;
     private Category category;
     private String bname;
     private String author;
     private double price;
     private double currPrice;
     private double discount;
     private String press;
     private String publishtime;
     private Integer edition;
     private Integer pageNum;
     private Integer wordNum;
     private String printtime;
     private Integer booksize;
     private String paper;
     private String imageW;
     private String imageB;
     private Integer orderBy;
     private Set cartitems = new HashSet(0);


    // Constructors

    /** default constructor */
    public Book() {
    }

	/** minimal constructor */
    public Book(Integer orderBy) {
        this.orderBy = orderBy;
    }
    
    /** full constructor */
    public Book(Category category, String bname, String author, double price, double currPrice, double discount, String press, String publishtime, Integer edition, Integer pageNum, Integer wordNum, String printtime, Integer booksize, String paper, String imageW, String imageB, Integer orderBy, Set cartitems) {
        this.category = category;
        this.bname = bname;
        this.author = author;
        this.price = price;
        this.currPrice = currPrice;
        this.discount = discount;
        this.press = press;
        this.publishtime = publishtime;
        this.edition = edition;
        this.pageNum = pageNum;
        this.wordNum = wordNum;
        this.printtime = printtime;
        this.booksize = booksize;
        this.paper = paper;
        this.imageW = imageW;
        this.imageB = imageB;
        this.orderBy = orderBy;
        this.cartitems = cartitems;
    }

   
    // Property accessors

    public String getBid() {
        return this.bid;
    }
    
    public void setBid(String bid) {
        this.bid = bid;
    }

    public Category getCategory() {
        return this.category;
    }
    
    public void setCategory(Category category) {
        this.category = category;
    }

    public String getBname() {
        return this.bname;
    }
    
    public void setBname(String bname) {
        this.bname = bname;
    }

    public String getAuthor() {
        return this.author;
    }
    
    public void setAuthor(String author) {
        this.author = author;
    }

    public double getPrice() {
        return this.price;
    }
    
    public void setPrice(double price) {
        this.price = price;
    }

    public double getCurrPrice() {
        return this.currPrice;
    }
    
    public void setCurrPrice(double currPrice) {
        this.currPrice = currPrice;
    }

    public double getDiscount() {
        return this.discount;
    }
    
    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getPress() {
        return this.press;
    }
    
    public void setPress(String press) {
        this.press = press;
    }

    public String getPublishtime() {
        return this.publishtime;
    }
    
    public void setPublishtime(String publishtime) {
        this.publishtime = publishtime;
    }

    public Integer getEdition() {
        return this.edition;
    }
    
    public void setEdition(Integer edition) {
        this.edition = edition;
    }

    public Integer getPageNum() {
        return this.pageNum;
    }
    
    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getWordNum() {
        return this.wordNum;
    }
    
    public void setWordNum(Integer wordNum) {
        this.wordNum = wordNum;
    }

    public String getPrinttime() {
        return this.printtime;
    }
    
    public void setPrinttime(String printtime) {
        this.printtime = printtime;
    }

    public Integer getBooksize() {
        return this.booksize;
    }
    
    public void setBooksize(Integer booksize) {
        this.booksize = booksize;
    }

    public String getPaper() {
        return this.paper;
    }
    
    public void setPaper(String paper) {
        this.paper = paper;
    }

    public String getImageW() {
        return this.imageW;
    }
    
    public void setImageW(String imageW) {
        this.imageW = imageW;
    }

    public String getImageB() {
        return this.imageB;
    }
    
    public void setImageB(String imageB) {
        this.imageB = imageB;
    }

    public Integer getOrderBy() {
        return this.orderBy;
    }
    
    public void setOrderBy(Integer orderBy) {
        this.orderBy = orderBy;
    }

    public Set getCartitems() {
        return this.cartitems;
    }
    
    public void setCartitems(Set cartitems) {
        this.cartitems = cartitems;
    }
   








}