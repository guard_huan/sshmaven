package com.zh.pojo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Category entity. @author MyEclipse Persistence Tools
 */

public class Category  implements java.io.Serializable {
    // Fields    

     private String cid;
     private Category category ;
     private String cname;
     private String desc;
     private Integer orderBy;
     private List<Category> categories = new ArrayList<Category>(0);
     private Set books = new HashSet(0);


    // Constructors

    /** default constructor */
    public Category() {
    }

	/** minimal constructor */
    public Category(Integer orderBy) {
        this.orderBy = orderBy;
    }
    
    /** full constructor */
    public Category(Category category, String cname, String desc, Integer orderBy, List categories, Set books) {
        this.category = category;
        this.cname = cname;
        this.desc = desc;
        this.orderBy = orderBy;
        this.categories = categories;
        this.books = books;
    }

   
    // Property accessors

    public String getCid() {
        return this.cid;
    }
    
    public void setCid(String cid) {
        this.cid = cid;
    }

    public Category getCategory() {
        return this.category;
    }
    
    public void setCategory(Category category) {
        this.category = category;
    }

    public String getCname() {
        return this.cname;
    }
    
    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getDesc() {
        return this.desc;
    }
    
    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getOrderBy() {
        return this.orderBy;
    }
    
    public void setOrderBy(Integer orderBy) {
        this.orderBy = orderBy;
    }

   

   

	public List getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public Set getBooks() {
        return this.books;
    }
    
    public void setBooks(Set books) {
        this.books = books;
    }
   








}