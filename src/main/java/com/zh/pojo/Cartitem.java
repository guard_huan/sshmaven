package com.zh.pojo;

import java.math.BigDecimal;

/**
 * Cartitem entity. @author MyEclipse Persistence Tools
 */
 
public class Cartitem  implements java.io.Serializable {
    // Fields    
     private String cartItemId;
     private Book book;
     private User user;
     private Integer quantity;
     private Integer orderBy;        
  // 添加小计方法
 	public double getSubtotal() {
 		
    	  /*使用BigDecimal不会有误差
 		           要求必须使用String类型构造器
 		  */	 
 		BigDecimal b1 = new BigDecimal(book.getCurrPrice() + "");
 		BigDecimal b2 = new BigDecimal(quantity + "");
 		BigDecimal b3 = b1.multiply(b2);
 		return b3.doubleValue();
 	}
    // Constructors   

	/** default constructor */
    public Cartitem() {
    }

	/** minimal constructor */
    public Cartitem(Integer orderBy) {
        this.orderBy = orderBy;
    }
    
    /** full constructor */
    public Cartitem(Book book, User user, Integer quantity, Integer orderBy) {
        this.book = book;
        this.user = user;
        this.quantity = quantity;
        this.orderBy = orderBy;
    }

   
    // Property accessors

    public String getCartItemId() {
        return this.cartItemId;
    }
    
    public void setCartItemId(String cartItemId) {
        this.cartItemId = cartItemId;
    }

    public Book getBook() {
        return this.book;
    }
    
    public void setBook(Book book) {
        this.book = book;
    }

    public User getUser() {
        return this.user;
    }
    
    public void setUser(User user) {
        this.user = user;
    }

    public Integer getQuantity() {
        return this.quantity;
    }
    
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getOrderBy() {
        return this.orderBy;
    }
    
    public void setOrderBy(Integer orderBy) {
        this.orderBy = orderBy;
    }
   








}