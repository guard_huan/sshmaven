package com.zh.pojo;



/**
 * Admin entity. @author MyEclipse Persistence Tools
 */

public class Admin  implements java.io.Serializable {


    // Fields    

     private String adminId;
     private String adminname;
     private String adminpwd;


    // Constructors

    /** default constructor */
    public Admin() {
    }

    
    /** full constructor */
    public Admin(String adminname, String adminpwd) {
        this.adminname = adminname;
        this.adminpwd = adminpwd;
    }

   
    // Property accessors

    public String getAdminId() {
        return this.adminId;
    }
    
    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public String getAdminname() {
        return this.adminname;
    }
    
    public void setAdminname(String adminname) {
        this.adminname = adminname;
    }

    public String getAdminpwd() {
        return this.adminpwd;
    }
    
    public void setAdminpwd(String adminpwd) {
        this.adminpwd = adminpwd;
    }
   








}