package com.zh.pojo;

import java.util.HashSet;
import java.util.Set;


/**
 * User entity. @author MyEclipse Persistence Tools
 */

public class User  implements java.io.Serializable {


    // Fields    

     private String uid;
     private String loginname;
     private String loginpass;
     private String email;
     private boolean status;
     private String activationCode;
     private Set orders = new HashSet(0);
     private Set cartitems = new HashSet(0);


    // Constructors

    /** default constructor */
    public User() {
    }

    
    /** full constructor */
    public User(String loginname, String loginpass, String email, boolean status, String activationCode, Set orders, Set cartitems) {
        this.loginname = loginname;
        this.loginpass = loginpass;
        this.email = email;
        this.status = status;
        this.activationCode = activationCode;
        this.orders = orders;
        this.cartitems = cartitems;
    }

   
    // Property accessors

    public String getUid() {
        return this.uid;
    }
    
    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getLoginname() {
        return this.loginname;
    }
    
    public void setLoginname(String loginname) {
        this.loginname = loginname;
    }

    public String getLoginpass() {
        return this.loginpass;
    }
    
    public void setLoginpass(String loginpass) {
        this.loginpass = loginpass;
    }

    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }

    public boolean getStatus() {
        return this.status;
    }
    
    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getActivationCode() {
        return this.activationCode;
    }
    
    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public Set getOrders() {
        return this.orders;
    }
    
    public void setOrders(Set orders) {
        this.orders = orders;
    }

    public Set getCartitems() {
        return this.cartitems;
    }
    
    public void setCartitems(Set cartitems) {
        this.cartitems = cartitems;
    }
   








}