package com.zh.pojo;

import java.util.ArrayList;
import java.util.List;



/**
 * Order entity. @author MyEclipse Persistence Tools
 */

public class Order  implements java.io.Serializable {   
     private String oid;
     private User user;
     private String ordertime;
     private double total;
     private Integer status;
     private String address;
     private List<Orderitem> orderitems = new ArrayList<Orderitem>();


    // Constructors

    /** default constructor */
    public Order() {
    }

    
    /** full constructor */
    public Order(User user, String ordertime, double total, Integer status, String address, List<Orderitem> orderitems) {
        this.user = user;
        this.ordertime = ordertime;
        this.total = total;
        this.status = status;
        this.address = address;
        this.orderitems = orderitems;
    }

   
    // Property accessors

    public String getOid() {
        return this.oid;
    }
    
    public void setOid(String oid) {
        this.oid = oid;
    }

    public User getUser() {
        return this.user;
    }
    
    public void setUser(User user) {
        this.user = user;
    }

    public String getOrdertime() {
        return this.ordertime;
    }
    
    public void setOrdertime(String ordertime) {
        this.ordertime = ordertime;
    }

    public double getTotal() {
        return this.total;
    }
    
    public void setTotal(double total) {
        this.total = total;
    }

    public Integer getStatus() {
        return this.status;
    }
    
    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getAddress() {
        return this.address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }

    public List<Orderitem> getOrderitems() {
        return this.orderitems;
    }
    
    public void setOrderitems(List<Orderitem> orderitems) {
        this.orderitems = orderitems;
    }
   








}