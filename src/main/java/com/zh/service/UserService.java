package com.zh.service;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import com.zh.pojo.User;

public interface UserService {
    public void test();
    public Serializable add(User user);
    public void update(User user); 
    public List<User> findByName(String name);//按照用户名来查找
    public List<User> findByEmail(String email);//按照邮箱来查找
    public List<User> findByActivationCode(String activationCode);//按照激活码来寻找用户  
    public void validateCode(String verifyCode)throws IOException;//校验验证码
    
}
