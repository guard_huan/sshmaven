package com.zh.service;

import java.io.Serializable;

import com.zh.pager.PageBean;

import com.zh.pojo.Order;

public interface OrderService {
	public PageBean<Order> findByUid(String uid, int pc);// 通过uid实现分页查找用户的订单
	public Serializable save(Order o);// 保存一条订单
	public Order findByOid(String oid);//通过订单编号Oid来查找订单的详细信息
	public void updateOrder(Order order);//更新订单
}
