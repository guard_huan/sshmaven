package com.zh.service;

import java.util.List;

import com.zh.pager.PageBean;
import com.zh.pojo.Book;

public interface BookService {
     public PageBean<Book> findByCid(String cid,int pc);//按照分类号查找当前页中书籍列表
     public PageBean<Book> findByAuthor(String author,int pc);//按照作者查询
     public PageBean<Book> findByPress(String press,int pc);//按照出版社查询
     public PageBean<Book> findByBname(String bname,int pc);//按照书名模糊查询
     public Book findByBid(String bid);//按照图书编号查找图书的所有详细信息
}
