package com.zh.service;

import java.util.List;

import com.zh.pojo.Category;

public interface CategoryService {
	
	public List<Category> findAllFirstCategory();// 查找所有的一级分类
	public List<Category> findChildrenByPid(String pid);//由父类ID查找它所有的 子类
}
