package com.zh.service.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zh.dao.OrderDao;
import com.zh.pager.PageBean;
import com.zh.pager.PageConstants;
import com.zh.pojo.Order;
import com.zh.service.OrderService;

public class OrderServiceImpl implements OrderService {
	private OrderDao orderDao;

	public OrderDao getOrderDao() {
		return orderDao;
	}

	public void setOrderDao(OrderDao orderDao) {
		this.orderDao = orderDao;
	}

	@Override
	public PageBean<Order> findByUid(String uid, int pc) {
		PageBean<Order> pageBean=new PageBean<Order>();
		String hql="from Order o where o.user.uid=:uid";
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("uid", uid);		
		List<Order> orderList=orderDao.find(hql, params, pc, PageConstants.BOOK_PAGE_SIZE);
		pageBean.setBeanList(orderList);
		pageBean.setTr(orderDao.find(hql, params).size());
		return pageBean;
	}

	@Override
	public Serializable save(Order o) {
		orderDao.save(o);
		return null;
	}

	@Override
	public Order findByOid(String oid) {		
		String hql="from Order o where o.oid=:oid";
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("oid", oid);
		List<Order> orderList=orderDao.find(hql, params);		
		return orderList.get(0);
	}

	@Override
	public void updateOrder(Order order) {
		orderDao.update(order);
		
	}


}
