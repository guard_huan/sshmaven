package com.zh.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zh.dao.BookDao;
import com.zh.pager.PageBean;
import com.zh.pager.PageConstants;
import com.zh.pojo.Book;
import com.zh.service.BookService;


public class BookServiceImpl implements BookService{
     private BookDao bookDao;

	public BookDao getBookDao() {
		return bookDao;
	}

	public void setBookDao(BookDao bookDao) {
		this.bookDao = bookDao;
	}

	@Override
	public PageBean<Book> findByCid(String cid,int pc) {
		PageBean<Book> pageBean=new PageBean<Book>();
		String hql="from Book b where b.category.cid=:cid";
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("cid", cid);		
		List<Book> bookList=bookDao.find(hql, params, pc, PageConstants.BOOK_PAGE_SIZE);
		pageBean.setBeanList(bookList);
		pageBean.setTr(bookDao.find(hql, params).size());
		return pageBean;
	}

	@Override
	public Book findByBid(String bid) {
		/*Book book=new Book();*/
		String hql="from Book b where b.bid=:bid";
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("bid", bid);
		List<Book> bookList=bookDao.find(hql, params);
		if(bookList!=null && bookList.size()>0){
			 return bookList.get(0);
		}else{
			return null;
		}
	}

	@Override
	public PageBean<Book> findByAuthor(String author, int pc) {
		PageBean<Book> pageBean=new PageBean<Book>();
		String hql="from Book b where b.author=:author";
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("author", author);		
		List<Book> bookList=bookDao.find(hql, params, pc, PageConstants.BOOK_PAGE_SIZE);
		pageBean.setBeanList(bookList);
		pageBean.setTr(bookDao.find(hql, params).size());
		return pageBean;		
	}

	@Override
	public PageBean<Book> findByPress(String press, int pc) {
		PageBean<Book> pageBean=new PageBean<Book>();
		String hql="from Book b where b.press=:press";
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("press", press);		
		
		List<Book> bookList=bookDao.find(hql, params, pc, PageConstants.BOOK_PAGE_SIZE);
		pageBean.setBeanList(bookList);
		pageBean.setTr(bookDao.find(hql, params).size());
		
		return pageBean;
	}

	@Override
	public PageBean<Book> findByBname(String bname, int pc) {
		// TODO Auto-generated method stub
		return null;
	}
     
     
}
