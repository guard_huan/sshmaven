package com.zh.service.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zh.dao.CartitemDao;
import com.zh.pojo.Cartitem;
import com.zh.service.CartitemService;

public class CartitemServiceImpl implements CartitemService {
	private CartitemDao cartitemDao;

	public CartitemDao getCartitemDao() {
		return cartitemDao;
	}

	public void setCartitemDao(CartitemDao cartitemDao) {
		this.cartitemDao = cartitemDao;
	}

	@Override
	public List<Cartitem> myCart(String uid) {
		String hql="from Cartitem c where c.user.uid=:uid";
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("uid", uid);
		List<Cartitem> cartitemList=cartitemDao.find(hql, params);
		return cartitemList;
	}

	@Override
	public Cartitem findByUidAndBid(String uid, String bid) {
		String hql="from Cartitem c where c.user.uid=:uid and c.book.bid=:bid";
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("uid", uid);
		params.put("bid", bid);
		List<Cartitem> cartitemList=cartitemDao.find(hql, params);	
		if(cartitemList!=null && cartitemList.size()>0){
			return cartitemList.get(0);
		}else{
			return null;
		}		
	}

	@Override
	public void update(Cartitem cartitem) {
		cartitemDao.update(cartitem);
		
	}

	@Override
	public Serializable save(Cartitem cartitem) {	
		return cartitemDao.save(cartitem);
	}

	@Override
	public void deleteAll(String uid) {
		String hql="from Cartitem c where c.user.uid=:uid";
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("uid", uid);
		List<Cartitem> cartitemList=cartitemDao.find(hql, params);
		for(Cartitem cartitem:cartitemList){
			cartitemDao.delete(cartitem);
		}
		
	}

	@Override
	public void delete(String cartitemId) {
		String hql="from Cartitem c where c.cartItemId=:cartitemId";
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("cartitemId", cartitemId);
		List<Cartitem> cartitemList=cartitemDao.find(hql, params);
		cartitemDao.delete(cartitemList.get(0));
	}

	@Override
	public Cartitem findByCartitemId(String cartItemId) {
		String hql="from Cartitem c where c.cartItemId=:cartItemId";
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("cartItemId", cartItemId);
		List<Cartitem> cartitemList=cartitemDao.find(hql, params);	
		if(cartitemList!=null && cartitemList.size()>0){
			return cartitemList.get(0);
		}else{
			return null;
		}		
	}

}
