package com.zh.service.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.zh.dao.UserDao;
import com.zh.pojo.User;
import com.zh.service.UserService;

public class UserServiceImpl implements UserService {
    private UserDao userDao;
    
	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public void test() {
		// TODO Auto-generated method stub
        System.out.println("sssss");
	}

	public Serializable add(User user) {
		System.out.println("UserServiceImpl");
		
		return userDao.add(user);
	}

	@Override
	public List<User> findByName(String name) {
		String sql="from User u where u.loginname=:loginname";
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("loginname", name);		
		return userDao.find(sql, map);
	}

	@Override
	public List<User> findByEmail(String email) {
		String sql="from User u where u.email=:email";
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("email", email);		
		return userDao.find(sql, map);
	}
	
	@Override
	public List<User> findByActivationCode(String activationCode) {
		String sql="from User u where u.activationCode=:activationCode";
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("activationCode", activationCode);		
		return userDao.find(sql, map);
	}
	
	@Override
	public void validateCode(String verifyCode) throws IOException {
		HttpServletResponse response=ServletActionContext.getResponse();
    	HttpSession session=ServletActionContext.getRequest().getSession();
    	String idCode=(String) session.getAttribute("code");
    	if(idCode.equalsIgnoreCase(verifyCode)){
    		response.getWriter().println(true);
    	}else{
    		response.getWriter().println(false);
    	}
		
	}

	@Override
	public void update(User user) {
		userDao.update(user);
		
	}

	

}
