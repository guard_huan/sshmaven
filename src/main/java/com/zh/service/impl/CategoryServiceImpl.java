package com.zh.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.zh.dao.CategoryDao;
import com.zh.pojo.Category;
import com.zh.service.CategoryService;

public class CategoryServiceImpl implements CategoryService {
    CategoryDao categoryDao;
    
	
	public CategoryDao getCategoryDao() {
		return categoryDao;
	}


	public void setCategoryDao(CategoryDao categoryDao) {
		this.categoryDao = categoryDao;
	}


	@Override
	public List<Category> findAllFirstCategory() {
		String sql="from Category c where c.category is null";		
		List<Category> parents=categoryDao.find(sql);
		for(Category category:parents){
			List<Category> children=this.findChildrenByPid(category.getCid());
			category.setCategories(children);
			for(Category c:children){
				c.setCategory(category);
			}
		}
		return parents;
		
	}


	@Override
	public List<Category> findChildrenByPid(String pid) {
		String hql="from Category c where c.category.cid=:pid";
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("pid",pid);	
		return categoryDao.find(hql, params);
	}


	

}
