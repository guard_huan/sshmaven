package com.zh.service;

import java.io.Serializable;
import java.util.List;

import com.zh.pojo.Cartitem;

public interface CartitemService {
	  public Cartitem findByCartitemId(String cartitemId);//按照条目编号查找
      public List<Cartitem> myCart(String uid);//根据用户的ID号查找此用户的所有购物清单
      public Cartitem findByUidAndBid(String uid,String bid);//按照uid和bid查询购物车条目
      public void update(Cartitem cartitem);//修改购物车某条信息
      public Serializable save(Cartitem cartitem);//向购物车添加一条购物条目
      public void deleteAll(String uid);//删除某个用户的所有购物车条目
      public void delete(String cartitemId);//删除购物车中的指定购物条目
}
