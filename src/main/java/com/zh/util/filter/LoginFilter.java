package com.zh.util.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.zh.pojo.User;

public class LoginFilter implements Filter {

	public void destroy() {
		// TODO Auto-generated method stub
	}
	
	/*HttpServletRequest接口是ServletRequest接口的一个子接口，不过它遵循Http协议，所以比较适用于web开发中的Http请求*/
	  
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest req=(HttpServletRequest) request;
		User user=(User) req.getSession().getAttribute("sessionUser");
		if(user==null){
			req.setAttribute("code", "error");
			req.setAttribute("msg", "你还没有登录，不能访问，请先登陆");
			req.getRequestDispatcher("/msg.jsp").forward(req, response);
		}else{
			chain.doFilter(request, response);
		}
		
	}

	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
